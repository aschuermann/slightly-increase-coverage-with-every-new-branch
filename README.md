# slightly-increase-coverage-with-every-new-branch

This is what you need to adjust your git to slightly increase the coverage (up to a certain point) with every new branch.
It helps when you take over projects that have bad coverage and you want to ensure that over time things improve.