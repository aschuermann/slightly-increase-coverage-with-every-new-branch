#!/usr/bin/env node

console.info(`Attempting to slightly increase coverage thresholds!`);

const fileName = 'projects/frontend/karma-admin.conf.js'
var fs = require('fs');
var fileContent = fs.readFileSync(fileName).toString();

const config = [
    {
        label: 'Statements Threshold',
        max: 80,
        regex: /(statements:\s*)([\d\.]+)(,?)/gm,
        increaseBy : 0.01
    },
    {
        label: 'Lines Threshold',
        max: 80,
        regex: /(lines:\s*)([\d\.]+)(,?)/gm,
        increaseBy : 0.03
    },
    {
        label: 'Branches Threshold',
        max: 80,
        regex: /(branches:\s*)([\d\.]+)(,?)/gm,
        increaseBy : 0.02
    },
    {
        label: 'Functions Threshold',
        max: 80,
        regex: /(functions:\s*)([\d\.]+)(,?)/gm,
        increaseBy : 0.05
    },
];

function checkAndIncreaseValue(data, label, max, regex, increaseBy) {
    var currentValue = Number(regex.exec(data)[2]);
    if (currentValue < max) {
        newValue = (Number(currentValue + increaseBy)).toFixed(2);
        console.log(`- Increasing ${label} from ${currentValue} to ${newValue}`);
        data = data.replace(regex, '$1' + newValue + '$3');
    }
    return data;
}

var newFileContent = fileContent;

for(var i = 0; i < config.length; i++) {
    newFileContent = checkAndIncreaseValue(
        newFileContent,
        config[i].label,
        config[i].max,
        config[i].regex,
        config[i].increaseBy);
}

if (newFileContent !== fileContent) {
    fs.writeFile(fileName, newFileContent, function(err) {
        if(err) {
            return console.log(err);
        }
        console.info('Config (' + fileName + ') has been updated.');
    });
}

console.info(`That's all DOS`);
